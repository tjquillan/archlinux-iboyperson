#!/bin/bash

SCRIPT_DIR=$( dirname $( readlink -e $0 ) )
source "$SCRIPT_DIR/ci-library.sh"
source "$SCRIPT_DIR/sourceforge.sh"

mkdir -p "$REPODIR"

_log build_step "Deploying to SourceForge Repo"
_do sf_deploy
