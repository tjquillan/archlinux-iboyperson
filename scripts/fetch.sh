#!/bin/bash

SCRIPT_DIR=$( dirname $( readlink -e $0 ) )
source "$SCRIPT_DIR/ci-library.sh"

function fetch_dependencies() {
    local packages=($@)
    local aur_deps=()
    local depends makedepends
    for package in "${packages[@]}"; do	
	_package_info "${package}" depends makedepends
	for dependency in "${depends[@]}" "${makedepends[@]}"; do
	    dependency=${dependency%=*}
	    dependency=${dependency%>*}
	    dependency=${dependency%<*}
	    #_log message "Checking dependency: ${dependency}"
	    if ! pacman -Ss "${dependency}" > /dev/null; then
		#_log message "${dependency} is an AUR Dependency"
		aur_deps+=("${dependency}")
	    fi
	done
    done
    #_log message "AUR Dependencies: ${aur_deps[@]}"
    _do fetch_packages "${aur_deps[@]}"
    if [[ ${aur_deps[@]} ]]; then
	_do fetch_dependencies "${aur_deps[@]}"
    fi
}

AUR_PACKAGES=$(cat "aur-packages.txt")

_do fetch_packages $AUR_PACKAGES
_do fetch_dependencies $AUR_PACKAGES
